// Tests for auth_client.c.

#include <iostream>
#include <thread>

#include <netinet/in.h>
#include <stdlib.h>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "gtest/gtest.h"
extern "C" {
#include "auth_client.h"
#include "kvcodec.h"
}

using namespace std;

// A fake authentication server. It must implement the wire protocol
// though, so it's most of a functional server anyway.
class Server {
public:
  Server(const char*);
  ~Server();

  void run();
  void stop();

private:
  void create();
  void handle(int);
  string get_request(int);
  bool decode_request(string, string&, string&);
  bool send_response(int, string);

  const char *socket_path_;
  int server_;
  int buflen_;
  char *buf_;
};

Server::Server(const char *socket_path) : socket_path_(socket_path) {
  // setup variables
  buflen_ = 1024;
  buf_ = new char[buflen_ + 1];

  create();
}

Server::~Server() { delete[] buf_; }

void Server::create() {
  struct sockaddr_un server_addr;

  // setup socket address structure
  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sun_family = AF_UNIX;
  strncpy(server_addr.sun_path, socket_path_, sizeof(server_addr.sun_path) - 1);

  // create socket
  unlink(socket_path_);
  server_ = socket(PF_UNIX, SOCK_STREAM, 0);
  if (!server_) {
    perror("socket");
    exit(-1);
  }

  // call bind to associate the socket with the UNIX file system
  if (bind(server_, (const struct sockaddr *)&server_addr,
           sizeof(server_addr)) < 0) {
    perror("bind");
    exit(-1);
  }

  // convert the socket to listen for incoming connections
  if (listen(server_, SOMAXCONN) < 0) {
    perror("listen");
    exit(-1);
  }
}

void Server::run() {
  // setup client
  int client;
  struct sockaddr_in client_addr;
  socklen_t clientlen = sizeof(client_addr);

  while ((client = accept(server_, (struct sockaddr *)&client_addr,
                          &clientlen)) > 0) {
    handle(client);
  }
}

void Server::stop() {
  close(server_);
  unlink(socket_path_);
}

void Server::handle(int client) {
  while (1) {
    // get a request
    string request = get_request(client);
    // break if client is done or an error occurred
    if (request.empty()) {
      cerr << "server error: empty request" << endl;
      break;
    }
    string username, password;
    if (not decode_request(request, username, password)) {
      cerr << "server error: could not decode request" << endl;
      break;
    }
    string response = "status=\"error\"\r\n";
    if (username == "testuser" && password == "password")
      response = "status=\"ok\"\r\n";
    // send response
    bool success = send_response(client, response);
    // break if an error occurred
    if (not success) {
      cerr << "server error: error sending response" << endl;
      break;
    }
  }
  close(client);
}

string Server::get_request(int client) {
  string request = "";
  size_t n;
  // read until we get a newline
  while ((n = request.find("\r\n")) == string::npos) {
    int nread = recv(client, buf_, buflen_, 0);
    if (nread < 0) {
      return "";
    } else if (nread == 0) {
      // the socket is closed
      break;
    }
    // be sure to use append in case we have binary data
    request.append(buf_, nread);
  }
  return request.substr(0, n);
}

bool Server::decode_request(string request, string& username, string& password) {
  cerr << "server: request: " << request << endl;

  if (request.substr(0, 5) != "auth ")
    return false;

  // Copy the request body to a buffer since we have to modify it in-place.
  strcpy(buf_, request.c_str() + 5);

  kvcodec_decoder dec;
  if (kvcodec_decode_init(&dec, buf_, request.size() - 5) < 0)
    return false;
  for (;;) {
    char *key, *value;
    if (kvcodec_decode_read_pair(&dec, &key, &value) < 0)
      return false;
    if (key == NULL)
      break;
    if (!strcmp(key, "username"))
      username = value;
    else if (!strcmp(key, "password"))
      password = value;
  }
  return true;
}

bool Server::send_response(int client, string response) {
  // prepare to send response
  const char *ptr = response.c_str();
  int nleft = response.length();
  int nwritten;
  // loop to be sure it is all sent
  while (nleft) {
    if ((nwritten = send(client, ptr, nleft, 0)) < 0) {
      // an error occurred, so break out
      perror("write");
      return false;
    } else if (nwritten == 0) {
      // the socket is closed
      return false;
    }
    nleft -= nwritten;
    ptr += nwritten;
  }
  return true;
}

static const char *test_socket_path = ".test-socket";

class AuthClientTest : public ::testing::Test {
public:
  AuthClientTest() {
    server_ = new Server(test_socket_path);
    server_thread_ = thread([&]{ server_->run(); });

    ac_ = auth_client_new("service", test_socket_path, 0);
    assert(ac_ != NULL);
  }

  virtual ~AuthClientTest() {
    auth_client_free(ac_);
    server_->stop();
    server_thread_.join();
    delete server_;
  }

  Server *server_;
  thread server_thread_;
  auth_client_t ac_;
};

TEST_F(AuthClientTest, AuthOK) {
  int result;

  result = auth_client_authenticate(ac_, "testuser", "password", NULL,
                                    "127.0.0.1");
  EXPECT_EQ(AC_OK, result) << "authenticate() error: "
                           << auth_client_strerror(ac_, result);
}

TEST_F(AuthClientTest, ManyAuthOK) {
  int result;

  for (int i = 0; i < 10; i++) {
    result = auth_client_authenticate(ac_, "testuser", "password", NULL,
                                      "127.0.0.1");
    EXPECT_EQ(AC_OK, result) << "authenticate() error: "
                             << auth_client_strerror(ac_, result);
}
}

TEST_F(AuthClientTest, AuthFail) {
  int result;

  result = auth_client_authenticate(ac_, "testuser", "bad_password", NULL,
                                    "127.0.0.1");
  EXPECT_NE(AC_OK, result) << "authenticate() didn't fail";
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
