auth-pam
========

PAM module to authenticate against
[auth-server](https://git.autistici.org/id/auth).

The installed module is called *pam_authclient*, and provides the
*account* (a no-op) and *auth* PAM operations.

Example configuration:

    auth required pam_authclient.so

Optionally you can set the *auth_socket* parameter if the auth-server
is not listening on the default socket path (which is
`/run/auth/socket`).

The service name sent to the auth server will be the PAM service by
default, but you can override this with the *service* parameter.

# Installation

Prerequisites:
* GCC
* PAM development headers (package *libpam0g-dev* in Debian)

Install the module with the standard autotools procedure:

    $ ./configure
    $ make
    $ sudo make install

Pass *--with-pam-dir* to set the target PAM module directory explicitly.
