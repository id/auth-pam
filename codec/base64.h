#ifndef __auth_c_base64_H
#define __auth_c_base64_H 1

#include <stddef.h>

#define BASE64_ERR_BUFFER_TOO_SMALL -2
#define BASE64_ERR_DECODE -3

size_t kvcodec_base64_encode_size(size_t);
int kvcodec_base64_encode(unsigned char *, size_t *, const unsigned char *, size_t);
int kvcodec_base64_decode(unsigned char *, size_t *, const unsigned char *, size_t);

#endif
