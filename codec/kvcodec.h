#ifndef __simple_attr_codec_H
#define __simple_attr_codec_H 1

#include <stddef.h>

/*
 * Buffered string accumulator, to efficiently concatenate tokens to a
 * fixed-size output buffer.
 */
typedef struct {
  char *buf;
  int bufsz;
  int pos;
} accumulate_buffer;

typedef struct {
  accumulate_buffer buf;
  int first;
} kvcodec_encoder;

int kvcodec_encode_init(kvcodec_encoder *, char *, size_t);
char *kvcodec_encode_get_result(kvcodec_encoder *);
int kvcodec_encode_pair(kvcodec_encoder *, const char *, const char *);

typedef struct {
  char *buf;
  char *pos;
  char *eob;
} kvcodec_decoder;

int kvcodec_decode_init(kvcodec_decoder *, char *, size_t);
int kvcodec_decode_read_pair(kvcodec_decoder *, char **, char **);

#endif
