
#include <iostream>
#include <stdlib.h>
#include <string.h>

extern "C" {
#include "base64.h"
#include "kvcodec.h"
}

#include "gtest/gtest.h"

static const char *test_value1 = "value1";
static const char *test_value2 = "o)$YfMwbQTdzDRP7pj6Ag[VZFB,OyL9>hxlvNq5E1`3r/a&U0\"4n8mG2HsW\\uetI";

TEST(TestCase1, Test) {
  kvcodec_encoder enc;
  kvcodec_decoder dec;
  char buf1[1024];

  EXPECT_EQ(0, kvcodec_encode_init(&enc, buf1, sizeof(buf1)));
  EXPECT_EQ(0, kvcodec_encode_pair(&enc, "key1", test_value1));
  EXPECT_EQ(0, kvcodec_encode_pair(&enc, "key2", test_value2));

  char *result = kvcodec_encode_get_result(&enc);
  EXPECT_NE(nullptr, result);

  std::cout << "result: " << result << std::endl;
  
  EXPECT_EQ(0, kvcodec_decode_init(&dec, result, strlen(result)));
  for (;;) {
    char *key = NULL, *value = NULL;
    EXPECT_EQ(0, kvcodec_decode_read_pair(&dec, &key, &value));
    if (key == NULL)
      break;
    if (strcmp(key, "key1") == 0) {
      EXPECT_EQ(0, strcmp(value, test_value1))
        << "bad value for key1: " << value << ", expected: " << test_value1;
    } else if (strcmp(key, "key2") == 0) {
      EXPECT_EQ(0, strcmp(value, test_value2))
        << "bad value for key2: " << value << ", expected: " << test_value2;
    } else {
      EXPECT_TRUE(0) << "bad key: " << key;
    }
  }
}

static void fill_random_buf(unsigned char *buf, size_t n) {
  while (n--) {
    *buf++ = (unsigned char)(rand() & 0xff);
  }
}

TEST(TestCase2, TestBse64) {
  unsigned char buf1[1024], buf2[1024], buf3[1024];

  for (int i = 0; i < 1000000; i++) {
    size_t len = 7 + rand() % 200, dlen = sizeof(buf2), olen = sizeof(buf3);
    fill_random_buf(buf1, len);

    int r = kvcodec_base64_encode(buf2, &dlen, buf1, len);
    ASSERT_EQ(0, r)
      << "base64_encode(len=" << len << ") failed: " << r;

    r = kvcodec_base64_decode(buf3, &olen, buf2, dlen);
    ASSERT_EQ(0, r)
      << "base64_decode(len=" << len << ") failed: " << r;

    EXPECT_EQ(len, olen);
    EXPECT_EQ(0, memcmp(buf1, buf3, len));
  }
}
