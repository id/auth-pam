// Implement the simple-attr dict serialization protocol.

#include <stdio.h>
#include <string.h>

#include "base64.h"
#include "kvcodec.h"

static const char *chars_to_escape = "\" \r\n";

/*
 * Add a string to the buffer. Return -1 if there is no space
 * left to copy it safely.
 */
static int accum_add(accumulate_buffer *acc, const char *s) {
  int n = strlen(s);
  if (acc->pos + n >= acc->bufsz) {
    return -1;
  }
  strcpy(acc->buf + acc->pos, s);
  acc->pos += n;
  return 0;
}

/*
 * Add a string to the buffer, base64-encoding it. Return -1 if there
 * is no space left in the buffer to hold the encoded string.
 */
static int accum_add_base64(accumulate_buffer *acc, const char *s) {
  int r, n = strlen(s);
  size_t nenc = kvcodec_base64_encode_size(n);
  if (acc->pos + (int)nenc >= acc->bufsz) {
    return -1;
  }
  nenc++; /* account for terminating zero */
  if ((r = kvcodec_base64_encode((unsigned char *)(acc->buf + acc->pos),
                                 &nenc, (const unsigned char *)s, n)) < 0)
    return r;
  acc->pos += nenc;
  return 0;
}

/*
 * Encoder.
 */
int kvcodec_encode_init(kvcodec_encoder *encoder, char *buf, size_t sz) {
  encoder->buf.buf = buf;
  encoder->buf.bufsz = sz;
  encoder->buf.pos = 0;
  encoder->first = 1;
  return 0;
}

char *kvcodec_encode_get_result(kvcodec_encoder *encoder) {
  return encoder->buf.buf;
}

static int kvcodec_encode_key(kvcodec_encoder *encoder, const char *key) {
  int r;
  if ((r = accum_add(&encoder->buf, key)) < 0)
    return r;
  if ((r = accum_add(&encoder->buf, "=")) < 0)
    return r;
  return 0;
}

static int kvcodec_encode_quoted_string(kvcodec_encoder *encoder,
                                        const char *value) {
  int r;
  if ((r = accum_add(&encoder->buf, "\"")) < 0)
    return r;
  if ((r = accum_add(&encoder->buf, value)) < 0)
    return r;
  if ((r = accum_add(&encoder->buf, "\"")) < 0)
    return r;
  return 0;
}

static int kvcodec_encode_base64_string(kvcodec_encoder *encoder,
                                        const char *value) {
  return accum_add_base64(&encoder->buf, value);
}

static int kvcodec_encode_value(kvcodec_encoder *encoder,
                                const char *value) {
  if (strpbrk(value, chars_to_escape) == NULL) {
    return kvcodec_encode_quoted_string(encoder, value);
  }
  return kvcodec_encode_base64_string(encoder, value);
}

int kvcodec_encode_pair(kvcodec_encoder *encoder, const char *key,
                        const char *value) {
  int r;

  if (encoder->first) {
    encoder->first = 0;
  } else {
    if ((r = accum_add(&encoder->buf, " ")) < 0)
      return r;
  }

  if ((r = kvcodec_encode_key(encoder, key)) < 0)
    return r;
  if ((r = kvcodec_encode_value(encoder, value)) < 0)
    return r;
  return 0;
}

int kvcodec_decode_init(kvcodec_decoder *decoder, char *buf, size_t sz) {
  decoder->buf = buf;
  decoder->pos = buf;
  decoder->eob = buf + sz;
  return 0;
}

static int kvcodec_decode_getc(kvcodec_decoder *decoder, char *c) {
  if (decoder->pos >= decoder->eob)
    return -1;
  *c = *(decoder->pos)++;
  return 0;
}

static void kvcodec_decode_ungetc(kvcodec_decoder *decoder) {
  if (decoder->pos > decoder->buf)
    decoder->pos--;
}

static int kvcodec_decode_read_until(kvcodec_decoder *decoder, char sep,
                                     char **out) {
  char *s = (char *)memchr(decoder->pos, sep, decoder->eob - decoder->pos);
  if (s == NULL) {
    return -1;
  }
  *out = decoder->pos;
  *s++ = '\0';
  decoder->pos = s;
  return 0;
}

static int kvcodec_decode_read_until_or_eof(kvcodec_decoder *decoder,
                                            char sep, char **out) {
  char *s;
  if (decoder->pos >= decoder->eob)
    return -1;
  s = strchr(decoder->pos, sep);
  if (s == NULL) {
    s = decoder->eob;
  }
  *out = decoder->pos;
  *s++ = '\0';
  decoder->pos = s;
  return 0;
}

static int kvcodec_decode_read_key(kvcodec_decoder *decoder,
                                   char **key_out) {
  int r;
  char c;

  // Skip whitespace.
  while ((r = kvcodec_decode_getc(decoder, &c)) == 0 && c == ' ')
    ;
  if (r < 0)
    return r;
  kvcodec_decode_ungetc(decoder);

  return kvcodec_decode_read_until(decoder, '=', key_out);
}

static int kvcodec_decode_read_value(kvcodec_decoder *decoder,
                                     char **value_out) {
  int r;
  size_t n;
  char c, *base64;

  if ((r = kvcodec_decode_getc(decoder, &c)) < 0)
    return r;
  if (c == '"')
    return kvcodec_decode_read_until(decoder, '"', value_out);

  // Base64 value. Decode in-place.
  kvcodec_decode_ungetc(decoder);
  if ((r = kvcodec_decode_read_until_or_eof(decoder, ' ', &base64)) < 0)
    return r;
  n = strlen(base64);
  if ((r = kvcodec_base64_decode((unsigned char *)base64, &n,
                                 (unsigned char *)base64, n)) < 0)
    return r;
  *(base64 + n) = '\0';
  *value_out = base64;
  return 0;
}

int kvcodec_decode_read_pair(kvcodec_decoder *decoder, char **key_out,
                             char **value_out) {
  int r;

  // Signal expected EOF by setting outputs to NULL.
  if (decoder->pos >= decoder->eob) {
    *key_out = NULL;
    *value_out = NULL;
    return 0;
  }

  if ((r = kvcodec_decode_read_key(decoder, key_out)) < 0)
    return r;
  if ((r = kvcodec_decode_read_value(decoder, value_out)) < 0)
    return r;
  return 0;
}
