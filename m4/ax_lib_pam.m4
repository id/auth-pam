# AX_LIB_PAM()

AC_DEFUN([AX_LIB_PAM], [
  AC_SEARCH_LIBS([pam_set_data], [pam])
  AC_CHECK_FUNCS([pam_getenv pam_getenvlist pam_modutil_getpwnam])
  dnl AC_REPLACE_FUNCS([pam_syslog pam_vsyslog])
  AC_CHECK_HEADERS([security/pam_modutil.h], [],
      [AC_CHECK_HEADERS([pam/pam_modutil.h])])
  AC_CHECK_HEADERS([security/pam_appl.h], [],
      [AC_CHECK_HEADERS([pam/pam_appl.h], [],
          [AC_MSG_ERROR([No PAM header files found])])])
  AC_CHECK_HEADERS([security/pam_ext.h], [],
      [AC_CHECK_HEADERS([pam/pam_ext.h])])
  AC_CHECK_HEADERS([security/pam_modules.h])
  AC_CHECK_HEADERS([security/_pam_macros.h])
  RRA_HEADER_PAM_CONST

  AC_SUBST(PAMDIR, "/lib/security")
  AC_ARG_WITH(pam-dir,
    AC_HELP_STRING([--with-pam-dir=DIR],
                   [Where to install PAM module [[/lib/security]]]),
              [case "${withval}" in
              /*) PAMDIR="${withval}";;
              ./*|../*) AC_MSG_ERROR(Bad value for --with-pam-dir);;
              *)  PAMDIR="/lib/${withval}";;
              esac])
  AC_MSG_NOTICE([PAM installation path $PAMDIR])

])
