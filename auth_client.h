#ifndef __libauthclient_authclient_h
#define __libauthclient_authclient_h 1

struct auth_client;
typedef struct auth_client* auth_client_t;

/* Return values from auth_client_authenticate */
#define AC_OK                           0
#define AC_ERR_AUTHENTICATION_FAILURE  -1
#define AC_ERR_OTP_REQUIRED            -2
#define AC_ERR_PROTOCOL                -3
#define AC_ERR_SYSTEM                  -4

/*
 * Create a new auth client instance.
 *
 * @param service Service name
 * @param servers A comma-separated list of host:port auth server
 * addresses
 */
auth_client_t auth_client_new(const char *service, const char *servers, int timeout);

/*
 * Free all resources associated with an auth client instance.
 */
void auth_client_free(auth_client_t ac);

/*
 * Return a human readable error string.
 *
 * @param err Error code returned by one of the auth_client_* methods
 */
const char *auth_client_strerror(auth_client_t, int err);

/*
 * Authenticate a user.
 *
 * @param ac Auth client
 * @param username Username
 * @param password Password
 * @param otp_token OTP token, if present (as a string)
 * @param source_ip Source IP of the user, where available
 */
int auth_client_authenticate(auth_client_t ac,
                             const char *username,
                             const char *password,
                             const char *otp_token,
                             const char *source_ip);

#endif
