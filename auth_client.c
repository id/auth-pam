/* Client for the socket-based authentication server */

#define _GNU_SOURCE

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "kvcodec.h"
#include "auth_client.h"

// Size of the internal r/w buffer.
#define BUFSIZE 4096

// Socket timeout for all operations.
#define DEFAULT_SOCKET_TIMEOUT_SECONDS 10

static const char *default_socket_path = "/run/auth/socket";

struct auth_client {
  const char *socket_path;
  const char *service;
  int sys_errno;
  int timeout;
};

auth_client_t auth_client_new(const char *service, const char *socket_path, int timeout) {
  auth_client_t ac;

  ac = (auth_client_t)malloc(sizeof(struct auth_client));
  if (ac == NULL) {
    return NULL;
  }

  if (socket_path == NULL || *socket_path == '\0') {
    socket_path = default_socket_path;
  }
  ac->socket_path = socket_path;
  ac->service = service;
  ac->sys_errno = 0;
  if (timeout <= 0) {
    timeout = DEFAULT_SOCKET_TIMEOUT_SECONDS;
  }
  ac->timeout = timeout;
  return ac;
}

void auth_client_free(auth_client_t ac) {
  free(ac);
}

static int connect_socket(auth_client_t ac, int *out_sock) {
  struct sockaddr_un addr;
  struct timeval timeout;
  int sock;
  
  memset(&addr, 0, sizeof(struct sockaddr_un));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, ac->socket_path, sizeof(addr.sun_path)-1);

  if ((sock = socket(PF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0)) < 0) {
    ac->sys_errno = errno;
    return -1;
  }

  timeout.tv_sec = ac->timeout;
  timeout.tv_usec = 0;
  if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0)
    goto err;
  if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)) < 0)
    goto err;
  
  if (connect(sock, (const struct sockaddr *)&addr, sizeof(addr)) < 0)
    goto err;

  *out_sock = sock;
  return 0;

 err:
  ac->sys_errno = errno;
  close(sock);
  return -1;
}

static int write_request(int sock, char *buf, size_t n) {
  ssize_t r;
  while (n > 0) {
    r = send(sock, buf, n, MSG_NOSIGNAL);
    if (r < 0) {
      return AC_ERR_SYSTEM;
    }
    n -= r;
    buf += r;
  }
  return 0;
}

static ssize_t read_response(int sock, char *buf, size_t sz) {
  char *wp = buf, *end;
  size_t n = 0;
  ssize_t r;

  while (1) {
    end = (char *)memmem(buf, n, "\r\n", 2);
    if (end != NULL) {
      *end = '\0';
      return (ssize_t)(end - buf);
    }
    
    r = recv(sock, wp, sz, 0);
    if (r <= 0)
      return AC_ERR_SYSTEM;
    n += r;
    sz -= r;
  }
}

static int parse_auth_status(char *status) {
  if (!strcmp(status, "ok"))
    return AC_OK;
  else if (!strcmp(status, "insufficient_credentials"))
    return AC_ERR_OTP_REQUIRED;
  return AC_ERR_AUTHENTICATION_FAILURE;
}

int auth_client_authenticate(auth_client_t ac,
                             const char *username,
                             const char *password,
                             const char *otp_token,
                             const char *source_ip) {
  int sock = -1;
  int retval = AC_ERR_AUTHENTICATION_FAILURE;
  ssize_t n;
  kvcodec_encoder encoder;
  kvcodec_decoder decoder;

  // Use a single buffer on the stack for encoding and decoding.
  char buf[BUFSIZE];

  if (connect_socket(ac, &sock) < 0) {
    return AC_ERR_SYSTEM;
  }
  
  // Build the authentication request command.
  strcpy(buf, "auth ");
  if (kvcodec_encode_init(&encoder, buf + 5, sizeof(buf) - 8) < 0) {
    goto err;
  }
  if ((kvcodec_encode_pair(&encoder, "service", ac->service) < 0)
      || (kvcodec_encode_pair(&encoder, "username", username) < 0)
      || (kvcodec_encode_pair(&encoder, "password", password) < 0)) {
    goto err;
  }
  if (otp_token != NULL) {
    if (kvcodec_encode_pair(&encoder, "otp", otp_token) < 0) {
      goto err;
    }
  }
  if (source_ip != NULL) {
    if (kvcodec_encode_pair(&encoder, "device.remote_addr", source_ip) < 0) {
      goto err;
    }
  }
  strcat(buf, "\r\n");

  // Write request to the socket.
  if (write_request(sock, buf, strlen(buf)) < 0) {
    ac->sys_errno = errno;
    goto err;
  }

  // Read response.
  if ((n = read_response(sock, buf, sizeof(buf))) < 0) {
    ac->sys_errno = errno;
    goto err;
  }

  //fprintf(stderr, "auth response line is: %s\n", buf);

  if (kvcodec_decode_init(&decoder, buf, n) < 0) {
    goto err;
  }

  while (1) {
    char *key, *value;
    if (kvcodec_decode_read_pair(&decoder, &key, &value) < 0)
      goto err;
    if (key == NULL)
      break;
    //fprintf(stderr, "auth response key='%s', value='%s'\n", key, value);
    if (!strcmp(key, "status")) {
      retval = parse_auth_status(value);
      break;
    }
  }

  close(sock);
  return retval;

 err:
  close(sock);
  return AC_ERR_PROTOCOL;
}

const char *auth_client_strerror(auth_client_t ac, int err) {
  switch (err) {
  case AC_OK:
    return "Success";
  case AC_ERR_AUTHENTICATION_FAILURE:
    return "Authentication failure";
  case AC_ERR_OTP_REQUIRED:
    return "Authentication failure (2FA required)";
  case AC_ERR_PROTOCOL:
    return "Protocol error";
  case AC_ERR_SYSTEM:
    // Boo, use strerror_r.
    return (const char *)strerror(ac->sys_errno);
  default:
    return "Unknown error";
  }
}
